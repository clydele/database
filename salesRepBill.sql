-- MySQL dump 10.13  Distrib 5.7.42, for Linux (x86_64)
--
-- Host: localhost    Database: db_billing
-- ------------------------------------------------------
-- Server version	5.7.42-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_branch`
--

DROP TABLE IF EXISTS `tbl_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_branch`
--

LOCK TABLES `tbl_branch` WRITE;
/*!40000 ALTER TABLE `tbl_branch` DISABLE KEYS */;
INSERT INTO `tbl_branch` VALUES (1,'Main Branch');
/*!40000 ALTER TABLE `tbl_branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_delivery_receipt`
--

DROP TABLE IF EXISTS `tbl_delivery_receipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_delivery_receipt` (
  `DRNum` varchar(255) NOT NULL,
  `CustomerID` varchar(255) NOT NULL,
  `DeliveredTo` varchar(255) DEFAULT NULL,
  `TIN` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `DeliveryDate` varchar(255) DEFAULT NULL,
  `PONum` varchar(255) DEFAULT NULL,
  `Terms` varchar(255) DEFAULT NULL,
  `Status` varchar(255) NOT NULL,
  `Notes` varchar(255) NOT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DRNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_delivery_receipt`
--

LOCK TABLES `tbl_delivery_receipt` WRITE;
/*!40000 ALTER TABLE `tbl_delivery_receipt` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_delivery_receipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_dlt`
--

DROP TABLE IF EXISTS `tbl_dlt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_dlt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `Description` text,
  `Serial` text,
  `Mac` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_dlt`
--

LOCK TABLES `tbl_dlt` WRITE;
/*!40000 ALTER TABLE `tbl_dlt` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_dlt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_grir`
--

DROP TABLE IF EXISTS `tbl_grir`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_grir` (
  `GRIRNum` int(11) NOT NULL AUTO_INCREMENT,
  `PONumber` varchar(255) DEFAULT NULL,
  `DateDelivered` date DEFAULT NULL,
  `ReceivedBy` varchar(255) DEFAULT NULL,
  `CheckedBy` varchar(255) DEFAULT NULL,
  `InventoryTransactionNum` varchar(255) DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GRIRNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_grir`
--

LOCK TABLES `tbl_grir` WRITE;
/*!40000 ALTER TABLE `tbl_grir` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_grir` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_inventory`
--

DROP TABLE IF EXISTS `tbl_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ProdID` varchar(255) NOT NULL,
  `Name` text,
  `Description` text,
  `Price` double DEFAULT NULL,
  `Threshold` int(11) DEFAULT NULL,
  `Unit` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Storage` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Brand` varchar(255) DEFAULT NULL,
  `Mac` varchar(255) DEFAULT NULL,
  `Serial` varchar(255) DEFAULT NULL,
  `Quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Branch` varchar(255) NOT NULL,
  `Status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_inventory`
--

LOCK TABLES `tbl_inventory` WRITE;
/*!40000 ALTER TABLE `tbl_inventory` DISABLE KEYS */;
INSERT INTO `tbl_inventory` VALUES (1,'1','Switch','Network Switch',14000,5,'UNIT','Count',NULL,'1',NULL,NULL,NULL,NULL,NULL,3,'2024-06-25 14:49:51','2024-06-25 14:49:51','1',NULL),(2,'2','Computer','Computer',25000,5,'UNIT','Itemized','1','1','2','0','0','1233','1233',1,'2024-06-25 14:49:51','2024-06-25 14:49:51','1','Available');
/*!40000 ALTER TABLE `tbl_inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_inventory_transaction`
--

DROP TABLE IF EXISTS `tbl_inventory_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_inventory_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(255) DEFAULT NULL,
  `Personnel` varchar(255) DEFAULT NULL,
  `Notes` text,
  `Status` varchar(255) NOT NULL,
  `Branch` varchar(255) NOT NULL,
  `Location` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_inventory_transaction`
--

LOCK TABLES `tbl_inventory_transaction` WRITE;
/*!40000 ALTER TABLE `tbl_inventory_transaction` DISABLE KEYS */;
INSERT INTO `tbl_inventory_transaction` VALUES (1,'Stock In','JC Libatique','test','Checked','1','1','2024-06-25 10:07:49','2024-06-25 14:49:51');
/*!40000 ALTER TABLE `tbl_inventory_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_inventory_transaction_products`
--

DROP TABLE IF EXISTS `tbl_inventory_transaction_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_inventory_transaction_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ProdID` varchar(255) NOT NULL,
  `Name` text,
  `Description` text,
  `Price` double DEFAULT NULL,
  `Threshold` int(11) DEFAULT NULL,
  `Unit` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Storage` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Brand` varchar(255) DEFAULT NULL,
  `ReferenceNum` varchar(255) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Branch` varchar(255) NOT NULL,
  `Serial` varchar(255) DEFAULT NULL,
  `Mac` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_inventory_transaction_products`
--

LOCK TABLES `tbl_inventory_transaction_products` WRITE;
/*!40000 ALTER TABLE `tbl_inventory_transaction_products` DISABLE KEYS */;
INSERT INTO `tbl_inventory_transaction_products` VALUES (1,'1','Switch','Network Switch',14000,5,'UNIT','Count','0','1','0','0','0','1',3,'1',NULL,NULL,'2024-06-25 14:46:12','2024-06-25 14:46:12'),(2,'2','Computer','Computer',25000,5,'UNIT','Itemized','1','1','2','0','0','1',1,'1','1233','1233','2024-06-25 14:49:40','2024-06-25 14:49:40');
/*!40000 ALTER TABLE `tbl_inventory_transaction_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_invoice`
--

DROP TABLE IF EXISTS `tbl_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Customer` varchar(255) DEFAULT NULL,
  `CustomerID` varchar(255) DEFAULT NULL,
  `Address` text NOT NULL,
  `TIN` varchar(255) DEFAULT NULL,
  `AmountDue` double DEFAULT NULL,
  `Status` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Vatable` double DEFAULT NULL,
  `VAT` double DEFAULT NULL,
  `Notes` text,
  `Email` varchar(255) NOT NULL,
  `DueDate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_invoice`
--

LOCK TABLES `tbl_invoice` WRITE;
/*!40000 ALTER TABLE `tbl_invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_nap`
--

DROP TABLE IF EXISTS `tbl_nap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_nap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `Description` text,
  `Core` text,
  `Ports` varchar(255) DEFAULT NULL,
  `Landmark` text,
  `DLTCode` varchar(255) DEFAULT NULL,
  `DLTSlot` varchar(255) DEFAULT NULL,
  `PONCode` varchar(255) DEFAULT NULL,
  `PONSlot` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_nap`
--

LOCK TABLES `tbl_nap` WRITE;
/*!40000 ALTER TABLE `tbl_nap` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_nap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_new_invoicedetails`
--

DROP TABLE IF EXISTS `tbl_new_invoicedetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_new_invoicedetails` (
  `ItemNum` int(11) NOT NULL AUTO_INCREMENT,
  `InvoiceNum` varchar(11) DEFAULT NULL,
  `ProductCatalog` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Category` varchar(255) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Unit` varchar(255) NOT NULL,
  `LotNumber` varchar(255) DEFAULT NULL,
  `Expiry` date DEFAULT NULL,
  `UnitPrice` varchar(255) DEFAULT NULL,
  `Amount` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ItemNum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_new_invoicedetails`
--

LOCK TABLES `tbl_new_invoicedetails` WRITE;
/*!40000 ALTER TABLE `tbl_new_invoicedetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_new_invoicedetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pon`
--

DROP TABLE IF EXISTS `tbl_pon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `Description` text,
  `location` text,
  `color` text,
  `splitter` text,
  `core` text,
  `ports` int(11) DEFAULT NULL,
  `landmark` text,
  `DLTCode` varchar(255) DEFAULT NULL,
  `DLTSlot` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pon`
--

LOCK TABLES `tbl_pon` WRITE;
/*!40000 ALTER TABLE `tbl_pon` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pricelist`
--

DROP TABLE IF EXISTS `tbl_pricelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pricelist` (
  `HospitalID` varchar(255) DEFAULT NULL,
  `HospitalName` varchar(255) DEFAULT NULL,
  `CatNumber` varchar(255) DEFAULT NULL,
  `Price` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pricelist`
--

LOCK TABLES `tbl_pricelist` WRITE;
/*!40000 ALTER TABLE `tbl_pricelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pricelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_brand`
--

DROP TABLE IF EXISTS `tbl_product_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_brand`
--

LOCK TABLES `tbl_product_brand` WRITE;
/*!40000 ALTER TABLE `tbl_product_brand` DISABLE KEYS */;
INSERT INTO `tbl_product_brand` VALUES (1,'TPLINK','2024-06-20 13:53:41','2024-06-20 13:53:41'),(2,'TENDAss','2024-06-20 05:55:39','2024-06-20 06:09:06');
/*!40000 ALTER TABLE `tbl_product_brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_category`
--

DROP TABLE IF EXISTS `tbl_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_category`
--

LOCK TABLES `tbl_product_category` WRITE;
/*!40000 ALTER TABLE `tbl_product_category` DISABLE KEYS */;
INSERT INTO `tbl_product_category` VALUES (1,'Category 1','2024-06-20 13:55:35','2024-06-20 06:14:42','Category 1');
/*!40000 ALTER TABLE `tbl_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_location`
--

DROP TABLE IF EXISTS `tbl_product_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_location`
--

LOCK TABLES `tbl_product_location` WRITE;
/*!40000 ALTER TABLE `tbl_product_location` DISABLE KEYS */;
INSERT INTO `tbl_product_location` VALUES (1,'Room One','2024-06-20 13:55:15','2024-06-20 06:14:30','room1');
/*!40000 ALTER TABLE `tbl_product_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_manufacturer`
--

DROP TABLE IF EXISTS `tbl_product_manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_manufacturer`
--

LOCK TABLES `tbl_product_manufacturer` WRITE;
/*!40000 ALTER TABLE `tbl_product_manufacturer` DISABLE KEYS */;
INSERT INTO `tbl_product_manufacturer` VALUES (1,'TPLINK','2024-06-17 01:38:33','2024-06-20 06:12:57');
/*!40000 ALTER TABLE `tbl_product_manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_supplier`
--

DROP TABLE IF EXISTS `tbl_product_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_supplier` (
  `SupplierID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `TNumber` varchar(255) DEFAULT NULL,
  `FNumber` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`SupplierID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_supplier`
--

LOCK TABLES `tbl_product_supplier` WRITE;
/*!40000 ALTER TABLE `tbl_product_supplier` DISABLE KEYS */;
INSERT INTO `tbl_product_supplier` VALUES (1,'Test Supplier One','Test Supplier One Address','09123456789','09123456789','supplier@gmail.com','2024-06-20 13:22:56','2024-06-20 13:22:56'),(2,'Test Supplier Two','Address Two','12345','12345','supplier2@gmail.com','2024-06-20 05:23:01','2024-06-20 05:23:01');
/*!40000 ALTER TABLE `tbl_product_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_unit`
--

DROP TABLE IF EXISTS `tbl_product_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_unit`
--

LOCK TABLES `tbl_product_unit` WRITE;
/*!40000 ALTER TABLE `tbl_product_unit` DISABLE KEYS */;
INSERT INTO `tbl_product_unit` VALUES (1,'PIECE'),(2,'UNIT');
/*!40000 ALTER TABLE `tbl_product_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_products`
--

DROP TABLE IF EXISTS `tbl_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text,
  `Description` text,
  `Price` double DEFAULT NULL,
  `Threshold` int(11) DEFAULT NULL,
  `Unit` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `Supplier` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Brand` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_products`
--

LOCK TABLES `tbl_products` WRITE;
/*!40000 ALTER TABLE `tbl_products` DISABLE KEYS */;
INSERT INTO `tbl_products` VALUES (1,'Switch','Network Switch',14000,5,'UNIT','Count','1',NULL,'2','1','1','2024-06-25 14:43:37','2024-06-25 14:43:37'),(2,'Computer','Computer',25000,5,'UNIT','Itemized','0','0','0','0','0','2024-06-25 14:48:56','2024-06-25 14:48:56');
/*!40000 ALTER TABLE `tbl_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_services`
--

DROP TABLE IF EXISTS `tbl_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_services` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `Unit` varchar(255) DEFAULT NULL,
  `UnitPrice` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_services`
--

LOCK TABLES `tbl_services` WRITE;
/*!40000 ALTER TABLE `tbl_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_subscriber_info`
--

DROP TABLE IF EXISTS `tbl_subscriber_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_subscriber_info` (
  `CustomerID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerName` varchar(255) DEFAULT NULL,
  `Address` text,
  `Email` varchar(255) DEFAULT NULL,
  `MobileNumber` varchar(11) DEFAULT NULL,
  `TelephoneNumber` varchar(255) DEFAULT NULL,
  `TIN` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`CustomerID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_subscriber_info`
--

LOCK TABLES `tbl_subscriber_info` WRITE;
/*!40000 ALTER TABLE `tbl_subscriber_info` DISABLE KEYS */;
INSERT INTO `tbl_subscriber_info` VALUES (1,'Sample Subscriber','Kaybagal North, Tagaytay City, Cavite','systemadmin@gmail.com','09123456789','0','88888888','2024-06-25 06:06:10','2024-07-02 02:35:11');
/*!40000 ALTER TABLE `tbl_subscriber_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_subscription_details`
--

DROP TABLE IF EXISTS `tbl_subscription_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_subscription_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerID` varchar(255) DEFAULT NULL,
  `dateInstalled` date DEFAULT NULL,
  `dateConnected` date DEFAULT NULL,
  `statusCode` varchar(255) DEFAULT NULL,
  `zoneNumber` varchar(255) DEFAULT NULL,
  `installer` varchar(255) DEFAULT NULL,
  `packageType` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `monthlyRate` double DEFAULT NULL,
  `boxNumber` text,
  `boxSerial` text,
  `macNumber` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_subscription_details`
--

LOCK TABLES `tbl_subscription_details` WRITE;
/*!40000 ALTER TABLE `tbl_subscription_details` DISABLE KEYS */;
INSERT INTO `tbl_subscription_details` VALUES (1,'1','2024-06-25','2024-06-25','Connected','Zone 1','Personnel 1','300mbps','1 Year',4000,'0000','0000','0000','2024-06-25 06:06:10','2024-06-25 06:06:10');
/*!40000 ALTER TABLE `tbl_subscription_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_supplier_products`
--

DROP TABLE IF EXISTS `tbl_supplier_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_supplier_products` (
  `Catalog` varchar(255) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Unit` varchar(255) NOT NULL,
  `Price` varchar(255) NOT NULL,
  `SupplierID` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_supplier_products`
--

LOCK TABLES `tbl_supplier_products` WRITE;
/*!40000 ALTER TABLE `tbl_supplier_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_supplier_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_troubleshooting`
--

DROP TABLE IF EXISTS `tbl_troubleshooting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_troubleshooting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Concern` text,
  `Resolution` text,
  `PartsInstalled` text,
  `PartsForOrder` text,
  `Status` varchar(255) DEFAULT NULL,
  `MachineModel` varchar(255) DEFAULT NULL,
  `JNumber` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `ServiceReport` blob,
  `ServiceRendered` text,
  `MachineStatus` varchar(255) DEFAULT NULL,
  `ReportTag` varchar(255) DEFAULT NULL,
  `HospitalID` text NOT NULL,
  `HospitalName` text NOT NULL,
  `OBID` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_troubleshooting`
--

LOCK TABLES `tbl_troubleshooting` WRITE;
/*!40000 ALTER TABLE `tbl_troubleshooting` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_troubleshooting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Pword` varchar(255) DEFAULT NULL,
  `Position` varchar(255) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Gender` varchar(255) NOT NULL,
  `Nationality` varchar(255) NOT NULL,
  `CivilStatus` varchar(255) NOT NULL,
  `Address` text NOT NULL,
  `MobileNumber` varchar(255) NOT NULL,
  `DateHired` date NOT NULL,
  `username` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users`
--

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` VALUES (1,'System Administrator','systemadmin@gmail.com','1','IT Admin','1990-12-12','Male','Filipino','Single','Kaybagal North, Tagaytay City, Cavite, Philippines','09123456789','2024-01-01','admin','2024-06-11 06:36:59','2024-07-02 02:33:51');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-01-25  7:40:51
